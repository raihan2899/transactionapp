package com.maybank.transactionapp.entity;


import java.time.LocalDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

@Entity
@Table(name="TransferAmount")
public class TransferAmount {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="tanggal_kirim")
	private LocalDateTime tanggalKirim;
	
    @NotNull(message = "amount must not be null")
	private Double jumlahAmount;
	
	private Double fee = 0.00;
	
    @ManyToOne(fetch = FetchType.EAGER.LAZY)
    @JoinColumn(name="rekPengirim_id", referencedColumnName = "id")
    private Rekening rekPengirim;
    
    @ManyToOne(fetch = FetchType.EAGER.LAZY)
    @JoinColumn(name="rekPenerima_id", referencedColumnName = "id")
    private Rekening rekPenerima;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public LocalDateTime getTanggalKirim() {
		return tanggalKirim;
	}

	public void setTanggalKirim(LocalDateTime tanggalKirim) {
		this.tanggalKirim = tanggalKirim;
	}

	public Double getJumlahAmount() {
		return jumlahAmount;
	}

	public void setJumlahAmount(Double jumlahAmount) {
		this.jumlahAmount = jumlahAmount;
	}

	public Double getFee() {
		return fee;
	}

	public void setFee(Double fee) {
		this.fee = fee;
	}

	public Rekening getRekPengirim() {
		return rekPengirim;
	}

	public void setRekPengirim(Rekening rekPengirim) {
		this.rekPengirim = rekPengirim;
	}

	public Rekening getRekPenerima() {
		return rekPenerima;
	}

	public void setRekPenerima(Rekening rekPenerima) {
		this.rekPenerima = rekPenerima;
	}
	
    
}
