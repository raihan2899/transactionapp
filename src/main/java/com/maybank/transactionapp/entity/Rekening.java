package com.maybank.transactionapp.entity;

import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;



@Entity
@Table(name="Rekening")
public class Rekening {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@NotNull
	@Column(name="no_rekening")
	private String noRekening;
	
	private Double saldo = 1000000.0;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="nasabah_id", referencedColumnName = "id")
	private Nasabah nasabah;
	
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="provider_id", referencedColumnName = "id")
	private Provider provider;
    
    @OneToMany(mappedBy = "rekPengirim", fetch = FetchType.LAZY)
    private List<TransferAmount> transferPengirim;
    
    @OneToMany(mappedBy = "rekPenerima", fetch = FetchType.LAZY)
    private List<TransferAmount> transferPenerima;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Nasabah getNasabah() {
		return nasabah;
	}

	public void setNasabah(Nasabah nasabah) {
		this.nasabah = nasabah;
	}
	
    public String getNoRekening() {
		return noRekening;
	}

	public void setNoRekening(String noRekening) {
		this.noRekening = noRekening;
	}

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	public Provider getProvider() {
		return provider;
	}

	public void setProvider(Provider provider) {
		this.provider = provider;
	}

	public List<TransferAmount> getTransferPengirim() {
		return transferPengirim;
	}

	public void setTransferPengirim(List<TransferAmount> transferPengirim) {
		this.transferPengirim = transferPengirim;
	}

	public List<TransferAmount> getTransferPenerima() {
		return transferPenerima;
	}

	public void setTransferPenerima(List<TransferAmount> transferPenerima) {
		this.transferPenerima = transferPenerima;
	}


}
	