package com.maybank.transactionapp.entity;

import java.time.LocalDateTime;
import java.util.Date;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "history")
public class HistoryTransfer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String penerima;
    private String pengirim;
    private LocalDateTime dateNow;
	private Double jumlahAmount;
	public Double getJumlahAmount() {
		return jumlahAmount;
	}
	public void setJumlahAmount(Double jumlahAmount) {
		this.jumlahAmount = jumlahAmount;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPenerima() {
		return penerima;
	}
	public void setPenerima(String penerima) {
		this.penerima = penerima;
	}
	public String getPengirim() {
		return pengirim;
	}
	public void setPengirim(String pengirim) {
		this.pengirim = pengirim;
	}
	public LocalDateTime getDateNow() {
		return dateNow;
	}
	public void setDateNow(LocalDateTime dateNow) {
		this.dateNow = dateNow;
	}
    
}