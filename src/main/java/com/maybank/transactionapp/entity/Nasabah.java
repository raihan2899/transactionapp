package com.maybank.transactionapp.entity;

import java.sql.Date;
import java.util.List;



import org.hibernate.validator.constraints.NotEmpty;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

@Entity
@Table(name="Nasabah")
public class Nasabah {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@NotBlank
	@NotNull
	@Column(name="nama_rekening")
	private String namaRekening;
	
	@Column(name="tanggal_lahir")
	private Date tanggalLahir;
	
	@Column(name="no_identitas")
	private String noIdentitas;
	
	@Column(name="tipe_identitas")
	private String tipeIdentitas;

	@Email
	@Column(name="email")
	private String email;

	@Column(name="no_contact")
	private String noContact;
	
	@OneToMany(mappedBy="nasabah" ,fetch = FetchType.LAZY)
	private List<Rekening> listRekening;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNamaRekening() {
		return namaRekening;
	}

	public void setNamaRekening(String namaRekening) {
		this.namaRekening = namaRekening;
	}

	public Date getTanggalLahir() {
		return tanggalLahir;
	}

	public void setTanggalLahir(Date tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}

	public String getNoIdentitas() {
		return noIdentitas;
	}

	public void setNoIdentitas(String noIdentitas) {
		this.noIdentitas = noIdentitas;
	}

	public String getTipeIdentitas() {
		return tipeIdentitas;
	}

	public void setTipeIdentitas(String tipeIdentitas) {
		this.tipeIdentitas = tipeIdentitas;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNoContact() {
		return noContact;
	}

	public void setNoContact(String noContact) {
		this.noContact = noContact;
	}

	public List<Rekening> getListRekening() {
		return listRekening;
	}

	public void setListRekening(List<Rekening> listRekening) {
		this.listRekening = listRekening;
	}

}