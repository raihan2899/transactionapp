package com.maybank.transactionapp.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

import com.maybank.transactionapp.service.CustomerUserDetailService;

@Configuration
@EnableWebSecurity
public class MayAuth {
	
	@Autowired
	private CustomerUserDetailService customerUserDetailService;
	
	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(customerUserDetailService);
		authProvider.setPasswordEncoder(bCryptPasswordEncoder());
		return authProvider;
	}

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Bean
	public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
		
		httpSecurity.authorizeHttpRequests().requestMatchers("/bankProvider").hasAnyAuthority("admin");
		httpSecurity.authorizeHttpRequests().requestMatchers("/tambahRekening").hasAnyAuthority("customerservice");
		httpSecurity.authorizeHttpRequests().requestMatchers("/transaksi").hasAnyAuthority("operator");
		httpSecurity.authorizeHttpRequests().requestMatchers("/historyTransfer").hasAnyAuthority("operator");
		
		httpSecurity.authorizeHttpRequests().anyRequest().authenticated();
		httpSecurity.authorizeHttpRequests().and().formLogin();
		
		return httpSecurity.build();
		}
	

	
//	@Bean
//	public UserDetailsService userDetailsService(BCryptPasswordEncoder bCryptPasswordEncoder) {
//		InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
//		manager.createUser(User.withUsername("abc").password(bCryptPasswordEncoder.encode("1234"))
//				.roles("regular").build());
//		manager.createUser(User.withUsername("admin").password(bCryptPasswordEncoder.encode("1234"))
//				.roles("admin").build());
//		return manager;
//	}
}
