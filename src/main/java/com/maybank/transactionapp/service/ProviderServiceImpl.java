package com.maybank.transactionapp.service;

import java.util.List;
import java.util.Optional;

import jakarta.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maybank.transactionapp.entity.Provider;
import com.maybank.transactionapp.repository.ProviderRepo;

@Service
@Transactional
public class ProviderServiceImpl implements ProviderService{
	@Autowired
	private ProviderRepo providerRepo;

	@Override
	public List<Provider> getAll() {
		// TODO Auto-generated method stub
		return this.providerRepo.findAll();
	}

	@Override
	public void save(Provider provider) {
		// TODO Auto-generated method stub
		this.providerRepo.save(provider);
	}

	@Override
	public void delete(Provider provider) {
		// TODO Auto-generated method stub
		this.providerRepo.delete(provider);
	}

	@Override
	public Optional<Provider> getProviderById(Long id) {
		// TODO Auto-generated method stub
		return this.providerRepo.findById(id);
	}
}
