package com.maybank.transactionapp.service;

import java.util.List;
import java.util.Optional;

import com.maybank.transactionapp.entity.Rekening;
import com.maybank.transactionapp.entity.TransferAmount;

import jakarta.validation.Valid;

public interface TransferAmountService {
	public List<TransferAmount> getAll();
	public void save(TransferAmount transferAmount);
	public void delete(TransferAmount transferAmount);
	public Optional<TransferAmount> getTranferById(Long id);
	public void sendAmount(Rekening rekeningPengirim, Rekening rekeningPenerima, @Valid TransferAmount transfer);
	
}
