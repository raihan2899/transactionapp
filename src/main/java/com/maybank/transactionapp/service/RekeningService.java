package com.maybank.transactionapp.service;

import java.util.List;
import java.util.Optional;
import com.maybank.transactionapp.entity.Rekening;

public interface RekeningService {
	public List<Rekening> getAll();
	public void save(Rekening rekening);
	public void delete(Rekening rekening);
	public Optional<Rekening> getRekeningById(Long id);
    Rekening findByNoRekening(String noRekening);
}
