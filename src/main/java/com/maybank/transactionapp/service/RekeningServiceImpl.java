package com.maybank.transactionapp.service;

import java.util.List;
import java.util.Optional;

import jakarta.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maybank.transactionapp.entity.Rekening;
import com.maybank.transactionapp.repository.RekeningRepo;

@Service
@Transactional
public class RekeningServiceImpl implements RekeningService{

	@Autowired
	private RekeningRepo rekeningRepo;
	
	@Override
	public List<Rekening> getAll() {
		// TODO Auto-generated method stub
		return this.rekeningRepo.findAll();
	}

	@Override
	public void save(Rekening rekening) {
		// TODO Auto-generated method stub
		this.rekeningRepo.save(rekening);
	}

	@Override
	public void delete(Rekening rekening) {
		// TODO Auto-generated method stub
		this.rekeningRepo.delete(rekening);
	}

	@Override
	public Optional<Rekening> getRekeningById(Long id) {
		// TODO Auto-generated method stub
		return this.rekeningRepo.findById(id);
	}
	
    @Override
    public Rekening findByNoRekening(String noRekening) {
        return this.rekeningRepo.findByNoRekening(noRekening);
    }

}
