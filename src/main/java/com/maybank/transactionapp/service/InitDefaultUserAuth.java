package com.maybank.transactionapp.service;

import java.util.ArrayList;
import java.util.List;

import jakarta.annotation.PostConstruct;
import jakarta.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.maybank.transactionapp.entity.Role;
import com.maybank.transactionapp.entity.User;
import com.maybank.transactionapp.repository.RoleRepo;
import com.maybank.transactionapp.repository.UserRepo;

@Service
@Transactional
public class InitDefaultUserAuth {

	@Autowired
	private RoleRepo roleRepo;
	
	@Autowired
	private UserRepo userRepo;
	
	
//	@PostConstruct
//	public void index() {
//		// create role
//		Role roleCustomerService = new Role();
//		Role roleAdmin = new Role();
//		Role roleOperator = new Role();
//		
//		roleCustomerService.setRole("customerservice");
//		roleAdmin.setRole("admin");
//		roleOperator.setRole("operator");
//		
//		this.roleRepo.save(roleCustomerService);
//		this.roleRepo.save(roleAdmin);
//		this.roleRepo.save(roleOperator);
//		
//		List<Role> listOperator = new ArrayList<>();
//		List<Role> listAdmin = new ArrayList<>();
//		List<Role> listCustomerService = new ArrayList<>();
//		
//		listOperator.add(roleOperator);
//		listAdmin.add(roleAdmin);
//		listCustomerService.add(roleCustomerService);
//		
//		// create user
//		User userAdmin = new User();
//		userAdmin.setUsername("admin");
//		userAdmin.setEmail("admin@maybank.com");
//		userAdmin.setPassword(new BCryptPasswordEncoder().encode("12345"));
//		userAdmin.setRoles(listAdmin);
//
//		User userOperator = new User();
//		userOperator.setUsername("operator");
//		userOperator.setEmail("operatork@maybank.com");
//		userOperator.setPassword(new BCryptPasswordEncoder().encode("12345"));
//		userOperator.setRoles(listOperator);
//		
//		User userCservice = new User();
//		userCservice.setUsername("customerservice");
//		userCservice.setEmail("customerservice@maybank.com");
//		userCservice.setPassword(new BCryptPasswordEncoder().encode("12345"));
//		userCservice.setRoles(listCustomerService);
//		
//		this.userRepo.save(userAdmin);
//		this.userRepo.save(userOperator);
//		this.userRepo.save(userCservice);
//	}
}
