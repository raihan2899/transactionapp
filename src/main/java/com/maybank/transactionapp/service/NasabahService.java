package com.maybank.transactionapp.service;

import java.util.List;
import java.util.Optional;

import com.maybank.transactionapp.entity.Nasabah;

public interface NasabahService {
	public List<Nasabah> getAll();
	public void save(Nasabah nasabah);
	public void delete(Nasabah nasabah);
	public Optional<Nasabah> getNasabahById(Long id);
}
