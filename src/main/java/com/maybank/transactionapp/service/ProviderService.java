package com.maybank.transactionapp.service;

import java.util.List;
import java.util.Optional;

import com.maybank.transactionapp.entity.Provider;

public interface ProviderService {
	public List<Provider> getAll();
	public void save(Provider provider);
	public void delete(Provider provider);
	public Optional<Provider> getProviderById(Long id);
}
