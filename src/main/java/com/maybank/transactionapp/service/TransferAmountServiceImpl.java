package com.maybank.transactionapp.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import jakarta.transaction.Transactional;
import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maybank.transactionapp.entity.Rekening;
import com.maybank.transactionapp.entity.TransferAmount;
import com.maybank.transactionapp.repository.TransferAmountRepo;

@Service
@Transactional
public class TransferAmountServiceImpl implements TransferAmountService{

	@Autowired
	private TransferAmountRepo transferAmountRepo;
	
	@Override
	public List<TransferAmount> getAll() {
		// TODO Auto-generated method stub
		return this.transferAmountRepo.findAll();
	}

	@Override
	public void save(TransferAmount transferAmount) {
		// TODO Auto-generated method stub
		this.transferAmountRepo.save(transferAmount);
	}

	@Override
	public void delete(TransferAmount transferAmount) {
		// TODO Auto-generated method stub
		this.transferAmountRepo.delete(transferAmount);
	}

	@Override
	public Optional<TransferAmount> getTranferById(Long id) {
		// TODO Auto-generated method stub
		return this.transferAmountRepo.findById(id);
	}

	@Override
	public void sendAmount(Rekening rekeningPengirim, Rekening rekeningPenerima, @Valid TransferAmount transfer) {
		// TODO Auto-generated method stub
//        Rekening rekPengirim = this.rekeningService.findByNoRekening(rekeningPengirim.getNoRekening());
//        Rekening rekPenerima = this.rekeningService.findByNoRekening(rekeningPenerima.getNoRekening());
//        Double takeAmount = transfer.getJumlahAmount();
//   	 
//        System.out.println("Nama Lengkap: "+rekPengirim);
//        System.out.println("Nama Lengkap: "+rekPenerima);
//        System.out.println("Nama Lengkap: "+takeAmount); 
//        
//        String bankPengirim = rekPengirim.getProvider().getName();
//        String bankPenerima = rekPenerima.getProvider().getName();
//        
//        // calculate
//        Double saldoPengirim = rekPengirim.getSaldo();
//        Double saldoPenerima = rekPenerima.getSaldo();
//        
//        
//        if (bankPengirim.equals(bankPenerima)) {
//       	 
//       	 Double minusSaldo = saldoPengirim - takeAmount;
//            Double plusSaldo = saldoPenerima + takeAmount;
//
//                rekPengirim.setSaldo(minusSaldo);
//                rekPenerima.setSaldo(plusSaldo);
//
//                this.rekeningService.save(rekPengirim);
//                this.rekeningService.save(rekPenerima);
//
//                //save ke transfer
//                LocalDateTime datetime = LocalDateTime.now();
//                transfer.setTanggalKirim(datetime);
//                transfer.setFee(0.0);
//                transfer.setRekPengirim(rekeningPengirim);
//                transfer.setRekPenerima(rekeningPenerima);
//                
//                historyTransfer.setPengirim(rekPengirim.getNoRekening());
//                historyTransfer.setPenerima(rekPenerima.getNoRekening());
//                historyTransfer.setDateNow(datetime);
//                historyTransfer.setJumlahAmount(transfer.getJumlahAmount());
//
//                attributes.addFlashAttribute("message", "Berhasil Transfer");
//                this.transferService.save(transfer);
//                this.historyTransferService.save(historyTransfer);
//
//            
//        }else {
//            Double minusSaldo = (saldoPengirim - takeAmount) - 6500.0;
//            Double plusSaldo = saldoPenerima + takeAmount;
//
//            // update saldo
//                rekPengirim.setSaldo(minusSaldo);
//                rekPenerima.setSaldo(plusSaldo);
//
//                this.rekeningService.save(rekPengirim);
//                this.rekeningService.save(rekPenerima);
//
//                //save ke transfer
//                LocalDateTime dateTime = LocalDateTime.now();
//
//                transfer.setTanggalKirim(dateTime);
//                transfer.setFee(6500.0);
//                transfer.setRekPengirim(rekeningPengirim);
//                transfer.setRekPenerima(rekeningPenerima);
//                
//                historyTransfer.setPengirim(rekPengirim.getNoRekening());
//                historyTransfer.setPenerima(rekPenerima.getNoRekening());
//                historyTransfer.setDateNow(dateTime);
//                historyTransfer.setJumlahAmount(transfer.getJumlahAmount());
//
//                attributes.addFlashAttribute("message", "Berhasil Transfer");
//                this.transferService.save(transfer);
//                this.historyTransferService.save(historyTransfer);
//   	 
//        }
	}

}
