package com.maybank.transactionapp.service;

import java.util.List;

import com.maybank.transactionapp.entity.HistoryTransfer;
import com.maybank.transactionapp.entity.Nasabah;

public interface HistoryTransferService {
	public List<HistoryTransfer> getAll();
	public void save(HistoryTransfer historyTransfer);
}
