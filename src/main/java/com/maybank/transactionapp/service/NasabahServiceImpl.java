package com.maybank.transactionapp.service;

import java.util.List;
import java.util.Optional;

import jakarta.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maybank.transactionapp.entity.Nasabah;
import com.maybank.transactionapp.repository.NasabahRepo;

@Service
@Transactional
public class NasabahServiceImpl implements NasabahService{

	@Autowired
	private NasabahRepo nasabahRepo;
	
	@Override
	public List<Nasabah> getAll() {
		// TODO Auto-generated method stub
		return this.nasabahRepo.findAll();
	}

	@Override
	public void save(Nasabah nasabahRepo) {
		// TODO Auto-generated method stub
		this.nasabahRepo.save(nasabahRepo);
	}

	@Override
	public void delete(Nasabah nasabahRepo) {
		// TODO Auto-generated method stub
		this.nasabahRepo.delete(nasabahRepo);
	}

	@Override
	public Optional<Nasabah> getNasabahById(Long id) {
		// TODO Auto-generated method stub
		return this.nasabahRepo.findById(id);
	}

}
