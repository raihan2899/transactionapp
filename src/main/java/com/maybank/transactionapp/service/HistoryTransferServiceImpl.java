package com.maybank.transactionapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maybank.transactionapp.entity.HistoryTransfer;
import com.maybank.transactionapp.repository.HistoryTransferRepo;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class HistoryTransferServiceImpl implements HistoryTransferService{

	@Autowired
	private HistoryTransferRepo historyTransferRepo;
	
	@Override
	public void save(HistoryTransfer historyTransfer) {
		// TODO Auto-generated method stub
		this.historyTransferRepo.save(historyTransfer);
	}

	@Override
	public List<HistoryTransfer> getAll() {
		// TODO Auto-generated method stub
		return this.historyTransferRepo.findAll();
	}

}
