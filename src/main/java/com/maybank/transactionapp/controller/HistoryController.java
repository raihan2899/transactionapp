package com.maybank.transactionapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.maybank.transactionapp.entity.HistoryTransfer;
import com.maybank.transactionapp.entity.Nasabah;
import com.maybank.transactionapp.service.HistoryTransferService;
import com.maybank.transactionapp.service.RekeningService;

@Controller
@RequestMapping("/historyTransfer")
public class HistoryController {

	@Autowired
	private HistoryTransferService historyTransferService;
	
	@GetMapping
	public String index(Model model) { 
		
		List<HistoryTransfer> listHistory = this.historyTransferService.getAll();
        model.addAttribute("listHistory", listHistory);
		return "historyTransfer";
	}
}
