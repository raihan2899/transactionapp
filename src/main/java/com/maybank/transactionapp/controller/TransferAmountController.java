package com.maybank.transactionapp.controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.maybank.transactionapp.entity.HistoryTransfer;
import com.maybank.transactionapp.entity.Provider;
import com.maybank.transactionapp.entity.Rekening;
import com.maybank.transactionapp.entity.TransferAmount;
import com.maybank.transactionapp.repository.TransferAmountRepo;
import com.maybank.transactionapp.service.HistoryTransferService;
import com.maybank.transactionapp.service.RekeningService;
import com.maybank.transactionapp.service.TransferAmountService;

import jakarta.validation.Valid;



@Controller
@RequestMapping("/transaksi")
public class TransferAmountController {

	@Autowired
	private RekeningService rekeningService;
	
	@Autowired
	private TransferAmountService transferService;
	
	@Autowired
	private HistoryTransferService historyTransferService;
	
    @GetMapping
    public String index(Model model) {
		List<Rekening> rekeningList = this.rekeningService.getAll();
        model.addAttribute("Rekening1", rekeningList);
        model.addAttribute("transfer", new TransferAmount());
        model.addAttribute("Rekening", new Rekening());
        model.addAttribute("Rekening2", new Rekening());
        
        List<TransferAmount> listTransaksi = this.transferService.getAll();
        model.addAttribute("listTransaksi", listTransaksi);
        return "transaksi";
    }
    
    @PostMapping("/save")
    public String save(@Valid @ModelAttribute("transfer") TransferAmount transfer,
            @ModelAttribute("Rekening") Rekening rekeningPengirim,
            @ModelAttribute("Rekening2") Rekening rekeningPenerima,
            @ModelAttribute("history") HistoryTransfer historyTransfer, BindingResult result,
            RedirectAttributes attributes,
            Model model) {
    	
    	 if (result.hasErrors()) {
    			List<Rekening> rekeningList = this.rekeningService.getAll();
    	        model.addAttribute("Rekening1", rekeningList);
    	        model.addAttribute("Rekening", rekeningPengirim);
    	        model.addAttribute("Rekening2", rekeningPenerima);
                model.addAttribute("transfer", transfer);
    	        
    	        List<TransferAmount> listTransaksi = this.transferService.getAll();
                model.addAttribute("listTransaksi", listTransaksi);
                return "transaksi";
    	 }
    	 
    	 // Logika (Harus Di Service)
//    	 this.transferService.sendAmount(rekeningPengirim, rekeningPenerima, transfer);
    	 
         Rekening rekPengirim = this.rekeningService.findByNoRekening(rekeningPengirim.getNoRekening());
         Rekening rekPenerima = this.rekeningService.findByNoRekening(rekeningPenerima.getNoRekening());
         Double takeAmount = transfer.getJumlahAmount();
    	 
         System.out.println("Nama Lengkap: "+rekPengirim);
         System.out.println("Nama Lengkap: "+rekPenerima);
         System.out.println("Nama Lengkap: "+takeAmount); 
         
         String bankPengirim = rekPengirim.getProvider().getName();
         String bankPenerima = rekPenerima.getProvider().getName();
         
         // calculate
         Double saldoPengirim = rekPengirim.getSaldo();
         Double saldoPenerima = rekPenerima.getSaldo();
         
         
         if (bankPengirim.equals(bankPenerima)) {
        	 
        	 Double minusSaldo = saldoPengirim - takeAmount;
             Double plusSaldo = saldoPenerima + takeAmount;

                 rekPengirim.setSaldo(minusSaldo);
                 rekPenerima.setSaldo(plusSaldo);

                 this.rekeningService.save(rekPengirim);
                 this.rekeningService.save(rekPenerima);

                 //save ke transfer
                 LocalDateTime datetime = LocalDateTime.now();
                 transfer.setTanggalKirim(datetime);
                 transfer.setFee(0.0);
                 transfer.setRekPengirim(rekeningPengirim);
                 transfer.setRekPenerima(rekeningPenerima);
                 
                 historyTransfer.setPengirim(rekPengirim.getNoRekening());
                 historyTransfer.setPenerima(rekPenerima.getNoRekening());
                 historyTransfer.setDateNow(datetime);
                 historyTransfer.setJumlahAmount(transfer.getJumlahAmount());

                 attributes.addFlashAttribute("message", "Berhasil Transfer");
                 this.transferService.save(transfer);
                 this.historyTransferService.save(historyTransfer);

             
         }else {
             Double minusSaldo = (saldoPengirim - takeAmount) - 6500.0;
             Double plusSaldo = saldoPenerima + takeAmount;

             // update saldo
                 rekPengirim.setSaldo(minusSaldo);
                 rekPenerima.setSaldo(plusSaldo);

                 this.rekeningService.save(rekPengirim);
                 this.rekeningService.save(rekPenerima);

                 //save ke transfer
                 LocalDateTime dateTime = LocalDateTime.now();

                 transfer.setTanggalKirim(dateTime);
                 transfer.setFee(6500.0);
                 transfer.setRekPengirim(rekeningPengirim);
                 transfer.setRekPenerima(rekeningPenerima);
                 
                 historyTransfer.setPengirim(rekPengirim.getNoRekening());
                 historyTransfer.setPenerima(rekPenerima.getNoRekening());
                 historyTransfer.setDateNow(dateTime);
                 historyTransfer.setJumlahAmount(transfer.getJumlahAmount());

                 attributes.addFlashAttribute("message", "Berhasil Transfer");
                 this.transferService.save(transfer);
                 this.historyTransferService.save(historyTransfer);
    	 
         }
         return "redirect:/transaksi";
    }
    
	@GetMapping("/delete")
	public String delete(@RequestParam("id") Long id) {
		Optional<TransferAmount> transferAmount = this.transferService.getTranferById(id);
		if(transferAmount.isPresent()) {
		this.transferService.delete(transferAmount.get());}
		return "redirect:/transaksi";
	}
    
	
}
