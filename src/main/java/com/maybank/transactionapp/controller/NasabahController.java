package com.maybank.transactionapp.controller;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.maybank.transactionapp.entity.Nasabah;
import com.maybank.transactionapp.entity.Provider;
import com.maybank.transactionapp.entity.Rekening;
import com.maybank.transactionapp.entity.TransferAmount;
import com.maybank.transactionapp.service.NasabahService;
import com.maybank.transactionapp.service.ProviderService;
import com.maybank.transactionapp.service.RekeningService;

import jakarta.validation.Valid;


@Controller
@RequestMapping("/tambahRekening")
public class NasabahController {
	
	@Autowired
	private NasabahService nasabahService;
	@Autowired
	private ProviderService providerService;
	@Autowired
	private RekeningService rekeningService;
	
	@GetMapping
	public String index(Model model) { 
		List<Provider> providers = this.providerService.getAll();
		List<Rekening> listRekening = this.rekeningService.getAll();
		List<Nasabah> listNasabah = this.nasabahService.getAll();
		
        model.addAttribute("providers", providers);
        model.addAttribute("listRekening", listRekening);
        model.addAttribute("listNasabah", listNasabah);
        
        model.addAttribute("provider", new Provider());
        model.addAttribute("nasabah", new Nasabah());
        model.addAttribute("rekening", new Rekening());
        
        return "tambahRekening";
	}
	
	@PostMapping("/save")
	public String save(@Valid @ModelAttribute("nasabah") Nasabah nasabah,Rekening rekening,Provider provider, BindingResult result, Model model){
		
			if(result.hasErrors()) {
		        model.addAttribute("provider", provider);
		        model.addAttribute("nasabah", nasabah);
		        model.addAttribute("rekening", rekening);
				return "tambahRekening";
			}
			
			System.out.println("Nama Lengkap: "+nasabah.getNamaRekening());
			System.out.println("No Identitas: "+nasabah.getNoIdentitas());
			System.out.println("Tipe Identitas: "+nasabah.getTipeIdentitas());
			System.out.println("No Kontak: "+nasabah.getNoContact());
			System.out.println("No Email: "+nasabah.getEmail());
			System.out.println("No Rekening: "+rekening.getNoRekening());
			System.out.println("Bank Provider: "+provider.getName());
			
			this.nasabahService.save(nasabah);
			rekening.setNasabah(nasabah);
			rekening.setProvider(provider);
			this.rekeningService.save(rekening);
			
			return "redirect:/tambahRekening";
		
	}
	
	@GetMapping("/delete")
	public String delete(@RequestParam("id") Long id) {
		Optional<Rekening> Rekenings = this.rekeningService.getRekeningById(id);
		if(Rekenings.isPresent()) {
		this.rekeningService.delete(Rekenings.get());}
		return "redirect:/tambahRekening";
	}
    
}
