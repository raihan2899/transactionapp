package com.maybank.transactionapp.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.maybank.transactionapp.entity.Provider;
import com.maybank.transactionapp.service.ProviderService;

@Controller
@RequestMapping("/bankProvider")
public class ProviderController {

	@Autowired
	private ProviderService bankProviderService;
	
	@GetMapping
	public String index(Model model) {
		List<Provider> bankproviders = this.bankProviderService.getAll();
        model.addAttribute("bankproviders", bankproviders);
        model.addAttribute("bankprovidersForm", new Provider());
        
        return "bankProvider";
	}
	
	@PostMapping("/save")
	public String save(Provider bankProvider, RedirectAttributes attributes) {
		this.bankProviderService.save(bankProvider);
		attributes.addFlashAttribute("success","Berhasil Insert");
		return "redirect:/bankProvider";
	}
	
	@GetMapping("/delete")
	public String delete(@RequestParam("id") Long id) {
		Optional<Provider> bankprovider = this.bankProviderService.getProviderById(id);
		if(bankprovider.isPresent()) {
		this.bankProviderService.delete(bankprovider.get());}
		return "redirect:/bankProvider";
	}

}
