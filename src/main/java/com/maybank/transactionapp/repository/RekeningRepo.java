package com.maybank.transactionapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.transactionapp.entity.Rekening;

public interface RekeningRepo extends JpaRepository<Rekening, Long>{
    Rekening findByNoRekening(String noRekening);
}