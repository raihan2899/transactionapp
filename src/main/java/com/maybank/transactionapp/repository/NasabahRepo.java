package com.maybank.transactionapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.transactionapp.entity.Nasabah;

public interface NasabahRepo extends JpaRepository<Nasabah, Long>{

}
