package com.maybank.transactionapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.transactionapp.entity.Provider;

public interface ProviderRepo extends JpaRepository<Provider, Long> {

	
}
