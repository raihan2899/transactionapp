package com.maybank.transactionapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.transactionapp.entity.Nasabah;
import com.maybank.transactionapp.entity.TransferAmount;

public interface TransferAmountRepo extends JpaRepository<TransferAmount, Long>{

}
