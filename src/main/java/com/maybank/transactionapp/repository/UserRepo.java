package com.maybank.transactionapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.transactionapp.entity.Role;
import com.maybank.transactionapp.entity.User;

public interface UserRepo extends JpaRepository<User, Long>{
	User findByUsername(String username);
}
