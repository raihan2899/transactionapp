package com.maybank.transactionapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.transactionapp.entity.HistoryTransfer;

public interface HistoryTransferRepo extends JpaRepository<HistoryTransfer, Long>{

}
